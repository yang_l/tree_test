package com.example.tree.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author YangLi
 * @Date 2024/3/6 9:37
 * @注释
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("company_department")
public class TestEntity {
    private Integer id;
    private String name;
    private String pid;
    @TableField(exist = false)
    private List<TestEntity> children;

}
