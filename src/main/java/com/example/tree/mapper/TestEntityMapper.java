package com.example.tree.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.tree.entity.TestEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author YangLi
 * @Date 2024/3/7 9:52
 * @注释
 */
@Mapper
public interface TestEntityMapper extends BaseMapper<TestEntity> {
}
