package com.example.tree.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.tree.entity.TestEntity;
import com.example.tree.mapper.TestEntityMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author YangLi
 * @Date 2024/3/11 17:11
 * @注释
 */
@RestController
public class Controller {

    @Resource
    private TestEntityMapper testEntityMapper;

    @RequestMapping("/treeTest")
    public List<TestEntity> list() {
        LambdaQueryWrapper<TestEntity> wrapper = Wrappers.lambdaQuery();
        List<TestEntity> testEntities = testEntityMapper.selectList(wrapper);
        return buildByRecursive(testEntities);
    }

    public static List<TestEntity> buildByRecursive(List<TestEntity> testEntities) {
        List<TestEntity> trees = new ArrayList<>();
        for (TestEntity testEntity : testEntities) {
            if ("".equals(testEntity.getPid()) || testEntity.getPid() == null) {
                trees.add(findChildren(testEntity, testEntities));
            }
        }
        return trees;
    }

    /**
     * 递归查找子节点
     */
    public static TestEntity findChildren(TestEntity testEntity, List<TestEntity> testEntitys) {
        for (TestEntity projectBasicInfoDTO2 : testEntitys) {
            if (String.valueOf(testEntity.getId()).equals(projectBasicInfoDTO2.getPid())) {
                if (testEntity.getChildren() == null) {
                    testEntity.setChildren(new ArrayList<>());
                }
                //是否还有子节点，如果有的话继续往下遍历，如果没有则直接返回
                testEntity.getChildren().add(findChildren(projectBasicInfoDTO2, testEntitys));
            }
        }
        return testEntity;
    }
}
